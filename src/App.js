import React, { useState } from "react";
import "sanitize.css";
import "./App.css";
import Nav from "./Components/Nav.js";
import Recipe from "./Components/Recipe.js";
import Data from "./Assets/recipes.json";

console.log(Data);

function App() {
  const [active, setActive] = useState(false);

  return (
    <div className="App">
      <header className="App-header">
        <Nav />
        <div className="container">
          {Data.recipes.map(recipe => (
            <Recipe
              img={recipe.image_url}
              title={recipe.title}
              calories={recipe.calories}
              protein={recipe.macros.protein}
              carbs={recipe.macros.carbs}
              fat={recipe.macros.fat}
              servingSize={recipe.servingSize}
              prepTime={recipe.prepTime}
              ingredients={recipe.ingredients}
              directions={recipe.directions}
              active={active}
              updateActive={() => {
                setActive(!active);
              }}
            />
          ))}
        </div>
      </header>
    </div>
  );
}

export default App;
