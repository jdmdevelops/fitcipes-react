import React from "react";
import hat from "../Assets/chef-hat.svg";

function Nav() {
  return (
    <header className="title">
      <a href="#/">
        Fitcipes
        <img className="logo" src={hat} alt="hat" width="40" />
      </a>
    </header>
  );
}

export default Nav;
