import React, { useState } from "react";

function Recipe(props) {
  const [clicked, setClicked] = useState(false);

  const handleClick = () => {
    props.updateActive();
    setClicked(!clicked);
  };

  let className = clicked ? "clicked" : "";
  if ((!clicked && !props.active) || (clicked && props.active)) {
    return (
      <div className={className + " recipe"} onClick={handleClick}>
        <img src={props.img} alt="picOfRecipe" className="recipe-img" />
        <div className="info">
          <div className="recipe-title">{props.title}</div>
          <div className="nutrition">
            <div className="calories">Calories: {props.calories}</div>
            <div className="macros">
              <div>Protein: {props.protein}g</div>
              <div>Carbs: {props.carbs}g</div>
              <div>Fat: {props.fat}g</div>
            </div>
          </div>
          {clicked ? (
            <div class="clicked-container">
              <div class="calories">Serving size: {props.servingSize}</div>
              <div class="time-to-prepare">
                Prep-time: {props.prepTime} minutes
              </div>
              <ul class="ingredients">
                {props.ingredients.map(ingredient => (
                  <li>{ingredient}</li>
                ))}
              </ul>
              <ol class="directions">
                {props.directions.map(step => (
                  <li>{step}</li>
                ))}
              </ol>
            </div>
          ) : null}
        </div>
      </div>
    );
  } else return <div></div>;
}

export default Recipe;
